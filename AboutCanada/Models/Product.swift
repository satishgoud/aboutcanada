//
//  Product.swift
//  AboutCanada
//
//  Created by Satish Akula on 1/19/20.
//  Copyright © 2020 ASG. All rights reserved.
//

import UIKit

struct Product {

    var productName : String
    var productImage : UIImage
    var productDesc : String
}
