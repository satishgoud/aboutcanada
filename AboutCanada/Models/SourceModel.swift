//
//  SourceModel.swift
//  AboutCanada
//
//  Created by Satish Akula on 1/19/20.
//  Copyright © 2020 ASG. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String:Any]

class SourceModel {
    
    var imageHref :String!
    var name :String!
    var description :String!
    
    init?(dictionary :JSONDictionary) {
        
        guard let imageHref = dictionary["imageHref"] as? String,
        let name = dictionary["title"] as? String,
            let description = dictionary["description"] as? String else {
                return nil
    }
        
    self.imageHref = imageHref
    self.name = name
    self.description = description
}
    
    init(viewModel :SourceViewModel) {
        
        self.imageHref = viewModel.imageHref
        self.name = viewModel.name
        self.description = viewModel.body
    }
    
}
