//
//  SourceListViewModel.swift
//  AboutCanada
//
//  Created by Satish Akula on 1/19/20.
//  Copyright © 2020 ASG. All rights reserved.
//

import Foundation
import UIKit

public typealias KeyValues<Type> = [String:Type]

class SourceListViewModel : NSObject {
    
    @objc dynamic private(set) var sourceViewModels :[SourceViewModel] = [SourceViewModel]()
    private var token :NSKeyValueObservation?
    var bindToSourceViewModels :(() -> ()) = {  }
    
    override init() {
        
        super.init()
        
        token = self.observe(\.sourceViewModels) { _,_ in
            self.bindToSourceViewModels()
        }
        
        // call populate sources
        populateSources(api: MainbaseURL)
    }
    
    func invalidateObservers() {
        self.token?.invalidate()
    }
    
    func populateSources(api: String) {
        call(api: api, parameters: [:], view: UIView.init()) { (data, message, statusCode) in
            if statusCode == .OK{
                if let response = data as? [String:Any]{
                    if let itemlist = response["rows"] as? [[String:Any]]{
                        self.sourceViewModels = itemlist.map(SourceViewModel.init)
                    }
                    
                    if let title = response["title"] as? String {
                        screenTitle = title
                        NotificationCenter.default.post(name: NSNotification.Name("updateScreenTitle"), object: nil)
                    }
                }
            }
        }
    }
}

class SourceViewModel : NSObject {
    
    var imageHref :String?
    var name :String
    var body :String
    
    init(imageHref: String, name :String, description: String) {
        self.imageHref = imageHref
        self.name = name
        self.body = description
    }
    
    init(source :SourceModel) {
        self.imageHref = source.imageHref
        self.name = source.name
        self.body = source.description
    }
    
    public init(keyValues: KeyValues<Any>){
    
        self.imageHref = keyValues.string("imageHref")
        self.name = keyValues.string("title")
        self.body = keyValues.string("description")
    }
}
