//
//  TableViewDataSource.swift
//  AboutCanada
//
//  Created by Satish Akula on 1/19/20.
//  Copyright © 2020 ASG. All rights reserved.
//

import Foundation
import UIKit

class TableViewDataSource<Cell :ProductCell,ViewModel> : NSObject, UITableViewDataSource {
    
    private var cellIdentifier :String!
    private var items :[ViewModel]!
    var configureCell :(ProductCell,ViewModel) -> ()
    
    init(cellIdentifier :String, items :[ViewModel], configureCell: @escaping (ProductCell,ViewModel) -> ()) {
        
        self.cellIdentifier = cellIdentifier
        self.items = items
        self.configureCell = configureCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as! ProductCell
        cell.selectionStyle = .none
        let item = self.items[indexPath.row]
        self.configureCell(cell,item)
        return cell
    }
}
