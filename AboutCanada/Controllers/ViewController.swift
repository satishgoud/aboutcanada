//
//  ViewController.swift
//  AboutCanada
//
//  Created by Satish Akula on 1/18/20.
//  Copyright © 2020 ASG. All rights reserved.
//

import UIKit
import SDWebImage
import Reachability

var screenTitle: String = ""

class ViewController: UIViewController {

    private var sourceListViewModel :SourceListViewModel!
    private var dataSource :TableViewDataSource<ProductCell,SourceViewModel>!
    private var myTableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height

        NotificationCenter.default.addObserver(self, selector: #selector(useOfNotificationCenterToSetTitle), name: NSNotification.Name.init("updateScreenTitle"), object: nil)

        
        myTableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight - barHeight))
        myTableView.register(ProductCell.self, forCellReuseIdentifier: "Cell")
        myTableView.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        myTableView.addSubview(refreshControl)
        self.view.addSubview(myTableView)
        
        if Reachability.isConnectedToNetwork(){
            self.updateUI()
        }else{
            self.showMessage("No Internet", "The network is not reachable")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func updateUI() {
        
        self.sourceListViewModel = SourceListViewModel()
                
        // setting up the bindings
        self.sourceListViewModel.bindToSourceViewModels = {
            self.updateDataSource()
        }
    }
    
    private func updateDataSource() {
        
        self.dataSource = TableViewDataSource(cellIdentifier: "Cell", items: self.sourceListViewModel.sourceViewModels) { cell, vm in

            cell.productNameLabel.text = vm.name
            cell.productDescriptionLabel.text = vm.body
            cell.productDescriptionLabel.numberOfLines = 0
            
            cell.productImage.sd_setShowActivityIndicatorView(true)
            cell.productImage.sd_setIndicatorStyle(.gray)
            cell.productImage.sd_setImage(with: URL(string: vm.imageHref!), placeholderImage: #imageLiteral(resourceName: "camera"))
        }
        
        self.myTableView.dataSource = self.dataSource
        self.myTableView.reloadData()
    }
    
    @objc func useOfNotificationCenterToSetTitle() {
        self.title = screenTitle
    }
    
    @objc func refresh(sender:AnyObject) {
       // Code to refresh table view
        updateUI()
        refreshControl.endRefreshing()
    }
}

