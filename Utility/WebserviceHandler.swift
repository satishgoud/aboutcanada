//
//
//  WebserviceHandler.swift
//  AboutCanada
//
//  Created by Satish Akula on 1/18/20.
//  Copyright © 2020 ASG. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import Cache
import SVProgressHUD

/**
 
For handle error connection
https://gist.github.com/marcboeren/c5bae523b1df8febc238
*/


public typealias JSONCallback = (Any?, String?, RequestStatus)->Void


public let MainbaseURL: String = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"


public enum RequestStatus: Int {
    case OK             = 200
    case noAccount      = 201
    case error          = 202
    case notVerified    = 203
    case tokenExpire    = 204
    case requestDenied  = 401
    case invalidRequest = 400
}

class Retrier: RequestRetrier {
    
    var defaultRetryCount = 4
    private  var requestsAndRetryCounts: [(Request, Int)] = []
    private  var lock = NSLock()
    
    private func index(request: Request) -> Int? {
        return requestsAndRetryCounts.firstIndex(where: { $0.0 === request })
    }
    
    func addRetryInfo(request: Request, retryCount: Int? = nil) {
        lock.lock() ; defer { lock.unlock() }
        guard index(request: request) == nil else {
            print("ERROR addRetryInfo called for already tracked request");
            return }
        
        requestsAndRetryCounts.append((request, retryCount ?? defaultRetryCount))
    }
    
    func deleteRetryInfo(request: Request) {
        lock.lock() ; defer { lock.unlock() }
        guard let index = index(request: request) else {
            print("ERROR deleteRetryInfo called for not tracked request");
            return }
        
        requestsAndRetryCounts.remove(at: index)
    }
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion){
        
        lock.lock() ; defer { lock.unlock() }
        
        guard let index = index(request: request) else { completion(false, 0); return }
        let (request, retryCount) = requestsAndRetryCounts[index]
        
        if retryCount == 0 {
            completion(false, 0)
        } else {
            requestsAndRetryCounts[index] = (request, retryCount - 1)
            completion(true, 0.5)
        }
    }
}


public func call(api: String, parameters: [String: Any], view: UIView?, completion: @escaping JSONCallback){

    let urlString = api

      print("URL - \(urlString)")
      print("parameters - \(parameters.debugDescription)")

    let header = ["Content-Type": "application/x-www-form-urlencoded"]

    guard let url = URL(string: urlString), var request = try? URLRequest(url: url, method: .post, headers: header) else{
        completion(nil, "URLRequest Error", .error)
        return
    }
    request.httpBody = parameters.map{ "\($0)=\($1)" }.joined(separator: "&").data(using: .utf8) //getData(of: parameters)
    request.cachePolicy = .reloadIgnoringCacheData
    view?.showHud()

    Alamofire.request(url, method: .post, parameters: [:], encoding: JSONEncoding.default, headers: nil)
        .validate(statusCode: 200..<600)
        .responseJSON { response in
            
        view?.hideHud()
        if let data = response.data {
            let responseStrInISOLatin = String(data: data, encoding: String.Encoding.isoLatin1)
            guard let modifiedDataInUTF8Format = responseStrInISOLatin?.data(using: String.Encoding.utf8) else {
                  print("could not convert data to UTF-8 format")
                  return
             }
            do {
                let responseJSONDict = try JSONSerialization.jsonObject(with: modifiedDataInUTF8Format) as! [String: Any]
                print(responseJSONDict)
                completion(responseJSONDict, "Success", RequestStatus(rawValue: 200)!)

            } catch {
                print(error)
            }
        }
    }
}

public func callCacheApi(api: String, parameters: [String: Any], view: UIView?, completion: @escaping JSONCallback){
    
    let urlString = api
     print("URL - \(urlString)")
     print("parameters - \(parameters.debugDescription)")
    
    view?.showHud()
    
    openResultLog = false
    cacheExpiryConfig(expiry: DaisyExpiry.seconds(1800))
    timeoutIntervalForRequest(60)

    request(urlString, params: parameters, dynamicParams :parameters).cache(true).responseCacheAndString { response in
        
        view?.hideHud()
        
        switch response.result {
        case .success(let string):
            
            let data = string.data(using: .utf8)!
            
            if let JSON = ((try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [String: Any?]) as [String : Any?]??){
                if JSON != nil {
                    let data = JSON?["response"] as? [String:Any] //extractData(JSON)
                    let code = JSON?["response_code"] as? Int ?? 202
                    let status = RequestStatus(rawValue: code) ?? RequestStatus.error
                    let message = JSON?["message"] as! String
                    
                    if status == .error{
                        completion(data, message, status)
                    }else{
                        completion(data, message, status)
                    }
                }
                else {
                    completion(nil, "No data found", RequestStatus.error)
                }
            }

        case .failure(let error):
            print(error)
            completion(nil, response.result.error!.localizedDescription, .error)
        }
    }
}

import SystemConfiguration

public class Reachability {

    class func isConnectedToNetwork() -> Bool {

        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }

        /* Only Working for WIFI
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired

        return isReachable && !needsConnection
        */

        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)

        return ret

    }
}

